#include <iostream>
using namespace std;

void Sums(int, int&, int&, int&);

int main(){
	int N, first = 0, second = 0, third = 0;
	char again = 'y';

	while (again == 'y' || again == 'Y'){
		cout << "Sum calculators, enter a number: ";
		cin >> N;

		Sums(N, first, second, third);

		cout << "1 to N is : " << first
			<< "\nN+23 to N+200 is: " << second
			<< "\x2N to 5n is: " << third << endl << endl;

		cout << "Type y or Y to go again: ";
		cin >> again;
	}
}

void Sums(int in, int& out1, int& out2, int& out3){
	out1 = 0, out2 = 0, out3 = 0; //reinitialize
	int min = (in + 23), max = (in + 100), x2 = (in*2), x5= (in*5);
	
	for (int i = 1; i <= in; i++)
		out1 += i;

	for (int i2 = min; i2 <= max; i2++)
		out2 += i2;

	for (int i3 = x2; i3 <= x5; i3++)
		out3 += i3;
}