#include <iostream>
using namespace std;

void GCD(int, int, int&);

int main(){
	char again = 'y';
	while (again == 'y' || again == 'Y'){
		int usr1, usr2, fetch = 0;
		cout << "GCD finder, enter 2 numbers: ";
		cin >> usr1 >> usr2;

		GCD(usr1, usr2, fetch);
		cout << "The GCD is: " << fetch << endl << endl;

		cout << "Do you want to rerun(y|Y)? ";
		cin >> again;
	}
}

void GCD(int i1, int i2, int& out){
	out = 0; //reinitialize
	int c = i1 % i2;

	while (c != 0)
	{
		i1 = i2;
		i2 = c;
		c = i1 % i2;
	}
	out = i2;

}