#include <iostream>
using namespace std;

void Sort(int, int, int, int, int&, int&, int&, int&);

int main(){
	int usr1, usr2, usr3, usr4;
	char again = 'y';

	while (again == 'y' || again == 'Y'){
		cout << "Number sorter, enter 4 numbers: ";
		cin >> usr1 >> usr2 >> usr3 >> usr4;
		
		int lrg1 = 0, lrg2 = 0, lrg3 = 0, lrg4 = 0;
		Sort(usr1, usr2, usr3, usr4, lrg1, lrg2, lrg3, lrg4);

		cout << "The largest number is: " << lrg1 << endl;
		cout << "The second largest number is: " << lrg2 << endl;
		cout << "The third largest number is: " << lrg3 << endl;
		cout << "The fourth largest number is: " << lrg4 << endl;

		system("pause");
		cout << "\n\nType y or Y to go again: \n\n";
		cin >> again;
	}
	return 0;
}

void Sort(int s1, int s2, int s3, int s4,
	int &_1lrg, int &_2lrg, int &_3lrg, int &_4lrg){
	
	if (s1 > s2 &&s1 > s3 &&s1 > s4){
		_1lrg = s1;
		if (s2 > s3&&s2 > s4){
			_2lrg = s2;
			if (s3 > s4){
				_3lrg = s3;
				_4lrg = s4;
			}
			else if (s4 > s3){
				_3lrg = s4;
				_4lrg = s3;
			}
			else cout << "\n\n s1/s2 if failed\n\n";
		}
		else if (s3 > s2 && s3 > s4){
			_2lrg = s3;
			if (s2 > s4){
				_3lrg = s2;
				_4lrg = s4;
			}
			else if (s4 > s2){
				_3lrg = s4;
				_4lrg = s2;
			}
			else cout << "\n\n s1/s3 if failed\n\n";
		}
		else if (s4 > s2&&s4 > s3){
			_2lrg = s4;
			if (s2 > s3){
				_3lrg = s2;
				_4lrg = s3;
			}
			else if (s3 > s2){
				_3lrg = s3;
				_4lrg = s2;
			}
			else cout << "\n\n s1/s4 if failed\n\n";
		}
		else cout << "\n\n s1 if failed\n\n";
	}


	if (s2 > s1 &&s2 > s3 &&s2 > s4){
		_1lrg = s2;
		if (s1 > s3&&s1 > s4){
			_2lrg = s1;
			if (s3 > s4){
				_3lrg = s3;
				_4lrg = s4;
			}
			else if (s4 > s3){
				_3lrg = s4;
				_4lrg = s3;
			}
			else cout << "\n\n s2/s1 if failed\n\n";
		}
		else if (s3 > s1 && s3 > s4){
			_2lrg = s3;
			if (s1 > s4){
				_3lrg = s1;
				_4lrg = s4;
			}
			else if (s4 > s1){
				_3lrg = s4;
				_4lrg = s1;
			}
			else cout << "\n\n s2/s3 if failed\n\n";
		}
		else if (s4 > s1&&s4 > s3){
			_2lrg = s4;
			if (s1 > s3){
				_3lrg = s1;
				_4lrg = s3;
			}
			else if (s3 > s1){
				_3lrg = s3;
				_4lrg = s1;
			}
			else cout << "\n\n s2/s4 if failed\n\n";
		}
		else cout << "\n\n s2 if failed\n\n";
	}

	if (s3 > s1 &&s3 > s2 &&s3 > s4){
		_1lrg = s3;
		if (s1 > s2&&s1 > s4){
			_2lrg = s1;
			if (s3 > s4){
				_3lrg = s3;
				_4lrg = s4;
			}
			else if (s4 > s3){
				_3lrg = s4;
				_4lrg = s3;
			}
			else cout << "\n\n s3/s1 if failed\n\n";
		}
		else if (s2 > s1 && s2 > s4){
			_2lrg = s2;
			if (s1 > s4){
				_3lrg = s1;
				_4lrg = s4;
			}
			else if (s4 > s1){
				_3lrg = s4;
				_4lrg = s1;
			}
			else cout << "\n\n s3/s2 if failed\n\n";
		}
		else if (s4 > s1&&s4 > s2){
			_2lrg = s4;
			if (s1 > s2){
				_3lrg = s1;
				_4lrg = s2;
			}
			else if (s2 > s1){
				_3lrg = s2;
				_4lrg = s1;
			}
			else cout << "\n\n s3/s4 if failed\n\n";
		}
		else cout << "\n\n s3 if failed\n\n";
	}

	if (s4 > s1 &&s4 > s2 &&s4 > s3){
		_1lrg = s4;
		if (s1 > s2&&s1 > s3){
			_2lrg = s1;
			if (s3 > s2){
				_3lrg = s3;
				_4lrg = s2;
			}
			else if (s2 > s3){
				_3lrg = s2;
				_4lrg = s3;
			}
			else cout << "\n\n s4/s1 if failed\n\n";
		}
		else if (s2 > s1 && s2 > s3){
			_2lrg = s2;
			if (s1 > s3){
				_3lrg = s1;
				_4lrg = s3;
			}
			else if (s3 > s1){
				_3lrg = s3;
				_4lrg = s1;
			}
			else cout << "\n\n s4/s2 if failed\n\n";
		}
		else if (s3 > s1&&s3 > s2){
			_2lrg = s3;
			if (s1 > s2){
				_3lrg = s1;
				_4lrg = s2;
			}
			else if (s2 > s1){
				_3lrg = s2;
				_4lrg = s1;
			}
			else cout << "\n\n s4/s3 if failed\n\n";
		}
		else cout << "\n\n s4 if failed\n\n";
	}
}